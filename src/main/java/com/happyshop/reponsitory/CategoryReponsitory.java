package com.happyshop.reponsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.happyshop.entity.Category;

public interface CategoryReponsitory extends JpaRepository<Category, Integer> {

}
