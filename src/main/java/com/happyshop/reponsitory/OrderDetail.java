package com.happyshop.reponsitory;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetail extends JpaRepository<OrderDetail, Integer> {

}
