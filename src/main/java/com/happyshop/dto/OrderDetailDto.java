package com.happyshop.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailDto {
	Integer id;
	// Integer orderId;
	// Integer productId;
	Double unitPrice;
	Integer quantity;
	Double discount;
}
