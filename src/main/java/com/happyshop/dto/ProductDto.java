package com.happyshop.dto;

import java.util.Date;
import java.util.List;

import com.happyshop.entity.Category;
import com.happyshop.entity.OrderDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
	Integer id;
	String name;
	Double unitPrice;
	String image;
	Date productDate;
	Boolean available;
	//Integer categoryId;
	Integer quantity;
	String description;
	Double discount;
	Integer viewCount;
	Boolean special;
	Category category;
	List<OrderDetail> orderDetails;
}
