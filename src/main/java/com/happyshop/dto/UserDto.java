package com.happyshop.dto;

import java.util.List;

import com.happyshop.entity.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
	String id;
	String password;
	String fullname;
	String telephone;
	String email;
	String photo;
	Boolean activated;
	Boolean admin;
	List<Order> orders;
}
