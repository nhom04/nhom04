package com.happyshop.dto;

import java.util.List;

import javax.persistence.OneToMany;

import com.happyshop.entity.Product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {
	Integer id;
	String name;
	String nameVN;
	List<Product> products;
}
