package com.happyshop.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrdeDto {
	private Date orderDate;
	private String telephone;
	private String address;
	private Double amount;
	private String description;
	private Integer status;

}
