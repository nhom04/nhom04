package com.happyshop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;



@EnableAutoConfiguration(exclude= {
		DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class,
		HibernateJpaAutoConfiguration.class	
})
@SpringBootApplication(scanBasePackages={"com.happyshop"})
@EnableJpaRepositories(basePackages = "com.happyshop.dao")
public class Nhom01Application {
	 private static final Logger LOGGER = LoggerFactory.getLogger(Nhom01Application.class);
	public static void main(String[] args) {
		SpringApplication.run(Nhom01Application.class, args);
		
	}
	

}
